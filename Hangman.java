public class Hangman {
	
	//Creates an empty variable for our "word" and sends it the runGame method since we can ONLY HAVE 1 SCANNER in the program. 
	public static void main(String[] args) {
		String word = ""; 
		runGame(word); 
	}
	
	    //Prompts the user to enter a 4 word character and user will start to guess whether a letter IS or IS NOT in the word. 
		// Game ends when the user misses 6 TIMES OR user GUESSES the word. 
		public static void runGame(String word) {
			
		//Creates boolean array representing the position of the word set to FALSE for now. 
		boolean[] positions = new boolean[4]; 
	
		for (int i = 0; i < positions.length; i++) {
			positions[i] = false; 
		} 
		
		int numberMisses = 0; 
		
		//Creates Scanner. User MUST input a 4 word character 
		java.util.Scanner reader = new java.util.Scanner(System.in); 
		System.out.println("Please enter a 4 word character: "); 
		word = reader.next(); 
		
		while (word.length() != 4) {
			System.out.println("Please enter a word with 4 characters.");
			word = reader.next(); 
		}
		
		
		while ((numberMisses < 6) && ((positions[0] == false) || (positions[1] == false) ||  (positions[2] == false) || (positions[3] == false))) {
			System.out.println(""); 
			System.out.println("Please enter a letter: ");
			String letter = reader.next();
			while (letter.length() != 1) {
				System.out.println("Please enter 1 letter only: ");
				letter = reader.next();
			}
			
	        //User MUST enter 1 letter only, which gets sent to the method "isLetterinWord" to check if the letter IS or IS NOT in the word. 
			char c = letter.charAt(0);
			int letterChecker = isLetterinWord(word, c);
			
			
			//IF the character is in the word the position in which it is in becomes TRUE and the method printWord is called 
			//IF the character IS NOT in the word the positions stay FALSE -> method printWord is called 
		      if (letterChecker != -1) {
				positions[letterChecker] = true; 
				printWord(word, positions);
			  } else {
				numberMisses++; 
				printWord(word, positions);
				System.out.println(""); 
				System.out.println("You missed " + numberMisses + " time(s).");
			  } 
			
		}	
		
		
		//If you miss 6 times -> YOU LOSE. If you guess the word -> YOU WIN!
		if (numberMisses == 6) {
			System.out.println("");
			System.out.println("You lost! The word was: " + word);
		} else {
			System.out.println(""); 
			System.out.println("You won! You guessed the word!"); 
		}
      
	}  
	
	// Checks whether a character inputted by the user is contained in the word user has to guess. 
	// Returns the position of the character in the word IF the character is contained in the word. IF NOT -> returns -1; (the character is NOT in the word) 
	// Uses the while loop to check every position of the word using INCREMENT. 
	public static int isLetterinWord(String word, char c) {
	  int loopCounter = 0;
	  while (loopCounter < 4) {
		  if (word.charAt(loopCounter) == c) {
			  return loopCounter;
		  }
		  loopCounter++; 
	  }
	  return -1; 
	}
	
	
	// IF the boolean is TRUE (the character is in 1 of the positions of the word), the character in that SPECIFIC POSITION is PRINTED 
	// IF the boolean is FALSE (for any of the positions of the word) that position will print "_" 
	// Checks every position of the word using a WHILE LOOP with INCREMENTS. 
	public static void printWord(String word, boolean[] letters) {
	  System.out.print("Your result is "); 
	  for (int i = 0; i < letters.length; i++) {
		  if (letters[i]) {
			  System.out.print(word.charAt(i));
		  } else {
		  	System.out.print("_"); 
		  } 
	  } 
	}
	
}